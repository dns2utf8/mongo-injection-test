const mongo = require("mongodb");
const sanitize = require("mongo-sanitize");
const bcrypt = require("bcrypt");

const saltRounds = 10;

let connection = undefined;

async function getConnection() {
    if (connection) {
        return connection;
    }
    throw new Error("Not connected");
}

async function getUserCollection() {
    const con = await getConnection();
    return con.db("demo").collection("user");
}

async function insertDefault() {
    const collection = await getUserCollection();
    const users = await collection.find();
    const count = await users.count();
    if (count === 0) {
        // don't ever use such defaults in production
        await db.insert("admin", "admin");
    }
}

const db = {
    connect: async (connectionString) => {
        connection = await mongo.connect(connectionString);
        await insertDefault();
    },
    insert: async (username, password) => {
        const collection = await getUserCollection();
        const pwHash = await bcrypt.hash(password, saltRounds);
        // never save plaintext PWs- except for demo purposes!!!
        await collection.insertOne({username, password, created: new Date(), passwordHash: pwHash});
    },
    login: async (username, password) => {
        const collection = await getUserCollection();
        // Query would not work like that when using hashes
        const cursor = await collection.find({username, password});
        return (await cursor.count()) === 1;
    },
    loginSanitized: async (username, password) => {
        // check if it is a string - cannot inject an object then
        if (typeof username !== "string") {
            return false;
        }
        // use a library to make sure nothing evil is contained in the parameter - useful if you need to pass objects
        const sanitizedPassword = sanitize(password);
        return db.login(username, sanitizedPassword);
    },
    loginCorrectly: async (username, password) => {
        try {
            if (typeof username !== "string") {
                return false;
            }
            const collection = await getUserCollection();
            const cursor = await collection.find({username});
            // there must be exactly one matching user to login
            if (await cursor.count() !== 1) {
                return false;
            }
            const user = await cursor.next();
            return await bcrypt.compare(password, user.passwordHash);
        } catch (e) {
            console.error(e);
            return false;
        }
    }
};

module.exports = db;