
run: ./node_modules
	npm start

lint:
	npm run lint
lint_fix:
	npm run lint-fix

start_mongodb_simple:
	docker run --rm --name injection-test-mongodb -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=examplePassword mongo:4.4-bionic

start_mongodb:
	docker run --rm --name injection-test-mongodb -p 27017:27017 --ip6 fd02:d0ce::db -v "$$(pwd)/mongo_datadir:/data/db" -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=examplePassword -it mongo:4.4-bionic

start_mongodb_v6:
	docker run --rm --name injection-test-mongodb -p 27017:27017 --ip6 fd02:d0ce::db -v "$$(pwd)/mongo_datadir:/data/db" -v "$$(pwd)/mongo_configdir:/data/config" -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=examplePassword -it mongo:4.4-bionic --config /data/config/mongod.conf

node_modules: setup
	#

setup:
	npm install

clean:
	rm -rf ./node_modules/