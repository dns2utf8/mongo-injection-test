const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const saltRounds = 10;


const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "user must have a username"],
        unique: true
    },
    password: {
        type: String,
        required: [true, "user must have a password"],
    },
    passwordHash: {
        type: String,
        required: [true, "user must have a password"],
    },
    created: {
        type: Date,
        required: [true, "user must have a creation time"],
    }
});
const userModel = mongoose.model("userMongoose", userSchema);

async function insertDefault() {
    const count = await userModel.count()
    if (count === 0) {
        // don't ever use such defaults in production
        await db.insert("admin", "admin");
    }
}


const db = {
    connect: async (connectionString) => {
        await mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});
        mongoose.connection.on("error", console.error.bind(console, "MongoDB connection error:"));

        await insertDefault();
    },
    insert: async (username, password) => {
        const pwHash = await bcrypt.hash(password, saltRounds);
        // never save plaintext PWs- except for demo purposes!!!
        const user = {username, password, created: new Date(), passwordHash: pwHash};

        await userModel.create(user)
    },

    login: async (username, password) => {
        try {
            const user = await userModel.findOne({username, password}).exec();
            return !!user;
        } catch (e) {
            console.error(e);
            return false;
        }
    },
    loginCorrectly: async (username, password) => {
        try {
            const user = await userModel.findOne({username});
            // there must be exactly one matching user to login
            if (!user) {
                return false;
            }
            return await bcrypt.compare(password, user.passwordHash);
        } catch (e) {
            console.error(e);
            return false;
        }
    }
};

module.exports = db;