const path = require("path");
const logger = require("morgan")("dev");
//const pug = require("pug");
const express = require("express");
const dbRaw = require("./dbRaw");
const dbMongoose = require("./dbMongoose");
const app = express();

app.use(logger);
// templates are in the `views` folder
app.set("view engine", "pug", {pretty: true,});

const mongoDB = "mongodb://root:examplePassword@localhost/admin";
dbRaw.connect(mongoDB).finally(() => "raw connection attempt finished");
dbMongoose.connect(mongoDB).finally(() => "mongoose connection attempt finished");


///////////////////////////////////////
///// Express.js
///////////////////////////////////////

// serve static files
app.use("/static", express.static(path.join(__dirname, "static")));

// Accept JSON
app.use(express.json());
// Accept multipart forms
app.use(express.urlencoded({extended: true}));

app.get("/hello", (req, res) => {
    res.send("Hello World!");
});

app.get("/", async (req, res) => {
    res.render("index", {
        title: "mongo-injection-test", message: "Exploit me!",
    });
});

app.post("/login", async (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    const loginRaw = await dbRaw.login(username, password);
    const loginSanitized = await dbRaw.loginSanitized(username, password);
    const loginCorrectly = await dbRaw.loginCorrectly(username, password);
    const loginMongoose = await  dbMongoose.login(username, password);
    const loginMongooseCorrectly = await  dbMongoose.loginCorrectly(username, password);
    res.render("loginResults", {
        title: "Login Results",
        loginRaw,
        loginSanitized,
        loginCorrectly,
        loginMongoose,
        loginMongooseCorrectly
    });
});

app.listen(3000, "::1");
console.log("startup complete");

